import styled from 'styled-components';
import { space, width, color, textAlign } from 'styled-system';

const Box = styled.div`
  overflow: hidden;
  ${space} ${width} ${color} ${textAlign};
`;

export default Box;
