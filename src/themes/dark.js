module.exports = {
    backgroundColour: '#41444d',
    textColour: '#e39ec1',
    textContrastColour: '#fff',
    textShadowColour: '#77567a',
    linkHighlightColour: '#77567a',
    lightBeamColour: 'rgba(250, 223, 32, 0.6)'
};
