module.exports = {
    backgroundColour: '#fff',
    textColour: '#333',
    textContrastColour: '#333',
    textShadowColour: 'hotpink',
    linkHighlightColour: 'hotpink',
    lightBeamColour: 'transparent'
};
